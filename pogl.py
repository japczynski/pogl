import sys

from PyQt4 import QtGui, QtCore, QtOpenGL, uic
from PyQt4 import QtOpenGL

from OpenGL.GL import *
import OpenGL.arrays.vbo as vbo
import numpy
import PIL.Image

import _transformations

vertexShaderCode = """
#version 330
layout(location = 0) in vec3 position;

uniform mat4 projectionMatrix;

void main()
{
    vec3 newPos = position + vec3(-1, 1, 0);
    gl_Position = projectionMatrix * vec4(newPos, 1.);
}
"""


fragmentShaderCode = """
#version 330
out vec4 out_color;
in vec2 UV;

void main()
{
    out_color = vec4(1., 1., 0., 1.);
}
"""


textureVertexShader = """
#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;

out vec2 UV;

void main()
{
    UV = vertexUV;
    gl_Position = vec4(position, 1.);
}
"""


textureFragmentShader = """
#version 330 core

// Input data
in vec2 UV;

// Ouput data
out vec3 color;

// Values that stay constant for the whole mesh
uniform sampler2D framebuffer;
uniform sampler2D multiplyTexture;

void main()
{
    float strength = 2.0;
    float zoom = 0.8;
    vec2 newUV = UV;

    float newX = UV.x - 0.5;
    float newY = UV.y - 0.5;

    float widthSquared = pow(0.5, 2.0);
    float heightSquared = pow(1.0, 2.0);
    float correctionRadius = sqrt(widthSquared + heightSquared) / strength;

    float distance = sqrt(pow(newX, 2.0) + pow(newY, 2.0));
    float r = distance / correctionRadius;

    float theta = 1;
    if (r > 0) {
        theta = tan(r) / r;
    }

    newUV.x = 0.5 + theta * newX * zoom;
    newUV.y = 0.5 + theta * newY * zoom;

    vec3 texColor = texture(framebuffer, newUV).rgb;
    vec3 multColor = texture(multiplyTexture, newUV).rgb;
    //color = texColor * multColor;
    //color = multColor;
    color = texColor;
}
"""


lbox = numpy.array([-1,  1, 0,
                     0,  1, 0,
                     0, -1, 0,
                    -1, -1, 0], dtype='float32')
rbox = numpy.array([ 0,  1, 0,
                     1,  1, 0,
                     1, -1, 0,
                     0, -1, 0], dtype='float32')
boxuv = numpy.array([0, 1,
                     1, 1,
                     1, 0,
                     0, 0], dtype='float32')

verts = numpy.array([# front face
                     -0.5,  0.5, 0.5,    # up left
                      0.5,  0.5, 0.5,    # up right
                      0.5, -0.5, 0.5,    # down right
                     -0.5, -0.5, 0.5,    # down left
                     #right side face
                      0.5,  0.5, 0.5,
                      0.5,  0.5, -0.5,
                      0.5, -0.5, -0.5,
                      0.5, -0.5, 0.5], dtype='float32')

# verts = numpy.array([-1, -1, 0,
#                       0,  1, 0,
#                       1, -1, 0], dtype='float32')


def compileShader(vertexShaderSource, fragmentShaderSource):
    vertexShader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertexShader, vertexShaderSource)
    glCompileShader(vertexShader)
    if not glGetShaderiv(vertexShader,GL_COMPILE_STATUS):
        print glGetShaderInfoLog(vertexShader)

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragmentShader, fragmentShaderSource)
    glCompileShader(fragmentShader)
    if not glGetShaderiv(fragmentShader,GL_COMPILE_STATUS):
        print glGetShaderInfoLog(fragmentShader)

    shader = glCreateProgram()
    glAttachShader(shader, vertexShader)
    glAttachShader(shader, fragmentShader)
    glLinkProgram(shader)

    return shader


def newFramebuffer(width, height):
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 width,
                 height,
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 GLvoidp(0))
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    framebuffer = glGenFramebuffers(1)
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer)
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0)
    glBindFramebuffer(GL_FRAMEBUFFER, 0)

    return framebuffer, texture


def createTestTexture():
    img = PIL.Image.open('UV_mapper.jpg')
    img_data = numpy.array(list(img.getdata()), numpy.int8)

    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 img.size[0],
                 img.size[1],
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 img_data)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    return texture


class Scene(QtOpenGL.QGLWidget):
    def __init__(self, parent):
        QtOpenGL.QGLWidget.__init__(self)

    def initializeGL(self):
        glClearColor(0, 0, 0, 0)
        print "OpenGL version %s" % glGetString(GL_VERSION)
        self.untexturedProgram = compileShader(vertexShaderCode,
                                               fragmentShaderCode)
        self.texturedProgram = compileShader(textureVertexShader,
                                             textureFragmentShader)
        self.triangleBuffer = vbo.VBO(verts)
        self.lquadBuffer = vbo.VBO(lbox)
        self.rquadBuffer = vbo.VBO(rbox)
        self.quadUVBuffer = vbo.VBO(boxuv)
        self.framebuffer, self.framebufferTexture = newFramebuffer(640, 480)
        self.testTexture = createTestTexture()
        self.projection = _transformations.clip_matrix(left=-2,
                                                       right=2,
                                                       bottom=-2,
                                                       top=2,
                                                       near=0.1,
                                                       far=100,
                                                       perspective=False)
        self.projection = numpy.matrix(self.projection, dtype='float32')

    def paintGL(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glBindFramebuffer(GL_FRAMEBUFFER, self.framebuffer)
        glBindTexture(GL_TEXTURE_2D, self.framebufferTexture)
        glUseProgram(self.untexturedProgram)

        projectionMatrixID = glGetUniformLocation(self.untexturedProgram,
                                                  "projectionMatrix")
        glUniformMatrix4fv(projectionMatrixID, 1, False, self.projection)

        self.triangleBuffer.bind()
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)
        glDrawArrays(GL_QUADS, 0, 8)
        glDisableVertexAttribArray(0)

        ####################

        for boxVBO in [self.lquadBuffer, self.rquadBuffer]:
            glBindFramebuffer(GL_FRAMEBUFFER, 0)
            glUseProgram(self.texturedProgram)

            # Set first texture sampler
            textureID = glGetUniformLocation(self.texturedProgram,
                                             "framebuffer")
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, self.framebufferTexture)
            glUniform1i(textureID, 0)

            # Set second texture input
            testTextureID = glGetUniformLocation(self.texturedProgram,
                                                 "multiplyTexture")
            glActiveTexture(GL_TEXTURE1)
            glBindTexture(GL_TEXTURE_2D, self.testTexture)
            glUniform1i(testTextureID, 1)

            glEnableVertexAttribArray(0)
            boxVBO.bind()
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)

            glEnableVertexAttribArray(1)
            self.quadUVBuffer.bind()
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, None)

            glDrawArrays(GL_QUADS, 0, 4)

            glDisableVertexAttribArray(0)
            glDisableVertexAttribArray(1)

    def resizeGL(self, width, height):
        """Called upon window resizing: reinitialize the viewport."""
        # update the window size
        self.width, self.height = width, height
        # paint within the whole window
        self.framebuffer, self.framebufferTexture = newFramebuffer(width,
                                                                   height)
        glViewport(0, 0, width, height)


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        uic.loadUi('test.ui', self)

        self.glWidget = Scene(self)
        self.centralwidget.layout().addWidget(self.glWidget)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    win = MainWindow()
    win.show()
    app.exec_()
